import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './web/home/home.component';
import { LoginComponent } from './web/login/login.component';
import { PageNotFoundComponent } from './web/page-not-found/page-not-found.component';
import { RegisterComponent } from './web/register/register.component';
import { ResetCredentialsComponent } from './web/reset-credentials/reset-credentials.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'reset-password', component: ResetCredentialsComponent },
    { path: 'home', component: HomeComponent },
    { path: '**', component: PageNotFoundComponent } // IMPORTANT: needs to be the last!
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}

export const routingComponents: any = [LoginComponent, PageNotFoundComponent];
