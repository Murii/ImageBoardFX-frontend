import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MatSliderModule } from '@angular/material/slider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './web/footer/footer.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './web/register/register.component';
import { ResetCredentialsComponent } from './web/reset-credentials/reset-credentials.component';
import { TopBarComponent } from './web/top-bar/top-bar.component';
import { HomeComponent } from './web/home/home.component';
import { AuthenticationService } from './services/authentication.service';
import { NewPostComponent } from './web/new-post/new-post.component';
import { PostComponent } from './web/home/post/post.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
    declarations: [
        AppComponent,
        routingComponents,
        FooterComponent,
        RegisterComponent,
        ResetCredentialsComponent,
        TopBarComponent,
        HomeComponent,
        NewPostComponent,
        PostComponent
    ],
    imports: [
        NgxPaginationModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatCardModule,
        MatPaginatorModule,
        MatToolbarModule,
        BrowserAnimationsModule,
        MatSliderModule,
        MatInputModule,
        MatChipsModule,
        MatListModule,
        MatFormFieldModule,
        MatIconModule,
        MatDividerModule,
        MatDialogModule,
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        BrowserAnimationsModule
    ],
    providers: [AuthenticationService],
    bootstrap: [AppComponent]
})
export class AppModule {}
