import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

/**
 * Simple class used to log things to stdout
 */
@Injectable({
    providedIn: 'root'
})
export class Logger {
    public log(msg: any) {
        console.log(msg);
    }

    public logError(msg: any, logOnProduction: boolean = false) {
        if (!environment.production || logOnProduction) {
            console.error(msg);
        }
    }
}
