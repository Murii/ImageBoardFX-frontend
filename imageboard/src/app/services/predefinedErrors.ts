export const ERR_UNEXPECTED_SERVER = 'Unexpected server error!';
export const ERR_INVALID_USERNAME_OR_PASSWORD = 'Invalid username or password';
