import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';
import { catchError, delay, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IPost } from '../web/home/post/post.interface';
import { BaseHTTPRequests } from './baseHTTPRequests';
import { Logger } from './logger.service';
import { ERR_UNEXPECTED_SERVER } from './predefinedErrors';

const NEW_POST_URL = environment.backendIpAddress + '/home/newPost';
const GET_POST_URL = environment.backendIpAddress + '/home/getPost';
const GET_POSTS_URL = environment.backendIpAddress + '/home/getPosts';
const GET_NUMBER_OF_POSTS_URL = environment.backendIpAddress + '/home/getNumberOfPost';

export class DataLoadingEvent {
    activateLoading: boolean;
    data: any;
}

@Injectable({
    providedIn: 'root'
})
export class ImageService extends BaseHTTPRequests {
    public showLoadingSpinner: EventEmitter<boolean> = new EventEmitter<boolean>();

    private headers: HttpHeaders = new HttpHeaders({
        'Access-Control-Allow-Methods': 'GET,PUT,OPTIONS,POST'
    });

    constructor(private http: HttpClient, private logger: Logger) {
        super();
    }

    public async getPosts(startIndex: number, limit: number): Promise<Array<IPost>> {
        let doneExecuting = false;
        this.headers = this.headers.set('Access-Control-Allow-Origin', GET_POSTS_URL);
        const getPostObeservable: Observable<any> = defer(() => {
            /**
             * The idea behaind this (it took me hours to figure this out) is:
             * We use the defer fuction above so we know when we have started
             * the observable from below.
             * With this information we know when we have started the request.
             * Now we are checking to see if after 200ms we're still waiting
             * on the request. If we do then we need to show the dialog until
             * it's finished
             */
            setTimeout(() => {
                if (!doneExecuting) {
                    this.showLoadingSpinner.emit(true);
                }
            }, 200);

            return this.http
                .get(GET_POSTS_URL + '/' + startIndex + '/' + limit, {
                    headers: this.headers
                })
                .pipe(
                    catchError(() => {
                        setTimeout(() => {
                            this.showLoadingSpinner.emit(false);
                        }, 200);
                        return Promise.reject('Failed to fetch more posts');
                    })
                );
        });
        return await getPostObeservable.toPromise().then((resp: Array<any>) => {
            doneExecuting = true;
            this.showLoadingSpinner.emit(false);
            return resp;
        });
    }

    public async getPost(id: number): Promise<any> {
        let doneExecuting = false;
        this.headers = this.headers.set('Access-Control-Allow-Origin', GET_POST_URL);
        const getPostObeservable: Observable<any> = defer(() => {
            setTimeout(() => {
                if (!doneExecuting) {
                    this.showLoadingSpinner.emit(true);
                }
            }, 200);

            return this.http.get(GET_POST_URL + '/' + id, { headers: this.headers }).pipe(
                catchError(() => {
                    setTimeout(() => {
                        this.showLoadingSpinner.emit(false);
                    }, 200);
                    return Promise.reject('Failed to fetch post');
                })
            );
        });

        return await getPostObeservable.toPromise().then((resp: any) => {
            doneExecuting = true;
            this.showLoadingSpinner.emit(false);
            //TODO:
            console.log('TODO: ', resp);
        });
    }

    public async getNumberOfPost(): Promise<number> {
        let doneExecuting = false;
        this.headers = this.headers.set(
            'Access-Contol-Allow-Origin',
            GET_NUMBER_OF_POSTS_URL
        );
        const getNumberOfPostss: Observable<any> = defer(() => {
            setTimeout(() => {
                if (!doneExecuting) {
                    this.showLoadingSpinner.emit(true);
                }
            }, 200);
            return this.http.get(GET_NUMBER_OF_POSTS_URL, { headers: this.headers }).pipe(
                catchError(() => {
                    setTimeout(() => {
                        this.showLoadingSpinner.emit(false);
                    }, 200);
                    return Promise.reject(0);
                })
            );
        });
        return await getNumberOfPostss.toPromise().then((resp: any) => {
            doneExecuting = true;
            this.showLoadingSpinner.emit(false);
            return resp;
        });
    }

    public async uploadPost(
        title: string,
        author: string,
        image: File,
        description: string,
        tags: Array<string>
    ): Promise<string> {
        let doneExecuting = false;
        const uploadImageData = new FormData();
      
        uploadImageData.append('imageFile', image, image.name);
        uploadImageData.append(
            'properties',
            new Blob(
                [
                    JSON.stringify({
                        title: title,
                        description: description,
                        tags: tags.join(),
                        author: author,
                    })
                ],
                {
                    type: 'application/json'
                }
            )
        );

        this.headers = this.headers.set('Access-Control-Allow-Origin', NEW_POST_URL);
        // this.headers = this.headers.set('Content-Type', 'multipart/form-data'); we don't have to do this because the browser will do it automagically and correctly
        const upload: Observable<any> = defer(() => {
            setTimeout(() => {
                if (!doneExecuting) {
                    this.showLoadingSpinner.emit(true);
                }
            }, 200);
            return this.http
                .post(NEW_POST_URL, uploadImageData, {
                    headers: this.headers,
                    observe: 'response'
                })
                .pipe(
                    catchError((err) => {
                        setTimeout(() => {
                            this.showLoadingSpinner.emit(false);
                        }, 200);
                        this.logger.logError(err);
                        return of(this.extractMessageFromResponse(err));
                    })
                );
        });
        return await upload.toPromise().then((resp: any) => {
            doneExecuting = true;
            this.showLoadingSpinner.emit(false);
            return resp === null
                ? ERR_UNEXPECTED_SERVER
                : this.extractMessageFromResponse(resp);
        });
    }
}
