import { ERR_UNEXPECTED_SERVER } from './predefinedErrors';

export class BaseHTTPRequests {
    /**
     * Parses the response objects from back-end then return the success messsage ('') or the error message.
     * @param response - the response object received from the back-end
     */
    public extractMessageFromResponse(response: any): string {
        if (typeof response === 'string') {
            return response;
        }
        if (response['body'] && response['body']['success'] === 1 || response['success'] === 1) {
            return '';
        }
        const errMsg: string = response['error'] ? response['error']['msg'] : undefined;
        if (errMsg !== undefined) {
            return errMsg;
        } else {
            return ERR_UNEXPECTED_SERVER;
        }
    }
}
