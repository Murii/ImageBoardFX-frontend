import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Logger } from './logger.service';
import { ERR_UNEXPECTED_SERVER } from './predefinedErrors';
import { BaseHTTPRequests } from './baseHTTPRequests';

const LOGIN_URL = environment.backendIpAddress + '/login';
const REGISTER_URL = environment.backendIpAddress + '/register';
const CHANGEPSWD_URL = environment.backendIpAddress + '/changepswd';
const DELETE_URL = environment.backendIpAddress + '/delete';
const PRE_CHANGE_PSWD_URL = environment.backendIpAddress + '/pre-changepswd';
const CONFIRM_REGISTRATION_URL = environment.backendIpAddress + '/confirm-registration';

export const JWT_LOCALSTORAGE_KEY = 'loginTokenKey';
/**
 * The associated username with the email after a successfull login
 */
export const USERNAME_LOCALSTORAGE_KEY = 'usernameKey';

//TODO: add in environment.ts the possibility to disable the jwt

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService extends BaseHTTPRequests {
    private headers: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json;charset=UTF-8',
        'Access-Control-Allow-Methods': 'GET,PUT,OPTIONS,POST'
    });

    constructor(private http: HttpClient, private logger: Logger) {
        super();
    }

    public async preChangePswd(email: string): Promise<string> {
        this.headers = this.headers.set(
            'Access-Control-Allow-Origin',
            PRE_CHANGE_PSWD_URL
        );
        const preChngPswdObservable: Observable<any> = this.http
            .post(PRE_CHANGE_PSWD_URL, { email: email }, { headers: this.headers })
            .pipe(
                catchError((err) => {
                    this.logger.logError(err);
                    return of(this.extractMessageFromResponse(err));
                })
            );
        return await preChngPswdObservable.toPromise().then((resp: any) => {
            return resp === null
                ? ERR_UNEXPECTED_SERVER
                : this.extractMessageFromResponse(resp);
        });
    }

    public async changePswd(
        email: string,
        newPassword: string,
        emailCode: string
    ): Promise<string> {
        this.headers = this.headers.set('Access-Control-Allow-Origin', CHANGEPSWD_URL);
        const changePswdObservable: Observable<any> = this.http
            .post(CHANGEPSWD_URL, {
                email: email,
                new_password: newPassword,
                email_code: emailCode
            })
            .pipe(
                catchError((err) => {
                    this.logger.logError(err);
                    return of(this.extractMessageFromResponse(err));
                })
            );
        return await changePswdObservable.toPromise().then((changePswd: any) => {
            return changePswd === null
                ? ERR_UNEXPECTED_SERVER
                : this.extractMessageFromResponse(changePswd);
        });
    }

    /**
     * Tries a silent login by checking if there's a jwt token and if that's so
     * it logs the user.
     */
    public async trySilentLogin(): Promise<string> {
        const jwtToken: string = localStorage.getItem(JWT_LOCALSTORAGE_KEY);
        if (jwtToken !== null) {
            return await this.login(null, null, jwtToken);
        }
        //TODO: return promise with undefined string or just return a rejected promise
        return undefined;
    }

    public async login(
        email: string,
        password: string,
        jwtToken: string = undefined
    ): Promise<string> {
        this.headers = this.headers.set('Access-Control-Allow-Origin', LOGIN_URL);
        // Do a silent login if we have a jwtToken
        if (jwtToken !== undefined) {
            return await this.loginWithJwt(jwtToken);
        }
        const loginObservable: Observable<any> = this.http
            .post(
                LOGIN_URL,
                { email: email, password: password },
                { headers: this.headers }
            )
            .pipe(
                catchError((err) => {
                    this.logger.logError(err);
                    return of(this.extractMessageFromResponse(err));
                })
            );
        return await loginObservable.toPromise().then((loginResponse: any) => {
            if (loginResponse === null) {
                return ERR_UNEXPECTED_SERVER;
            }
            /**
             * Check if the user has made a normal login with username & password
             * and if that's so extract the JWT token from backend and save it.
             */
            if (loginResponse['success'] === 1 && jwtToken === undefined) {
                /**
                 * We know for sure that when we do a login request
                 * and the reponse is successully we get a jwt token,
                 * so save it in local stoarage and not in the cookies
                 * so we avoid XSS attacks!
                 */
                localStorage.setItem(JWT_LOCALSTORAGE_KEY, loginResponse['msg']);
                // also save the username which we just recieved from the back-end associated with this email
                localStorage.setItem(USERNAME_LOCALSTORAGE_KEY, loginResponse['username']);
            }
            return this.extractMessageFromResponse(loginResponse);
        });
    }

    public async register(
        email: string,
        username: string,
        password: string
    ): Promise<string> {
        this.headers = this.headers.set('Access-Control-Allow-Origin', REGISTER_URL);
        const registerObservable: Observable<any> = this.http
            .put(
                REGISTER_URL,
                { email: email, username: username, password: password },
                { headers: this.headers }
            )
            .pipe(
                catchError((err) => {
                    this.logger.logError(err);
                    return of(this.extractMessageFromResponse(err));
                })
            );
        return await registerObservable.toPromise().then((registerResponse: any) => {
            return registerResponse === null
                ? ERR_UNEXPECTED_SERVER
                : this.extractMessageFromResponse(registerResponse);
        });
    }

    public async confirmRegistration(email: string, emailCode: string): Promise<string> {
        this.headers = this.headers.set(
            'Access-Control-Allow-Origin',
            CONFIRM_REGISTRATION_URL
        );
        const confirmRegistrationObservable: Observable<any> = this.http
            .post(
                CONFIRM_REGISTRATION_URL,
                { email: email, 'email-code': emailCode },
                { headers: this.headers }
            )
            .pipe(
                catchError((err) => {
                    this.logger.logError(err);
                    return of(this.extractMessageFromResponse(err));
                })
            );
        return await confirmRegistrationObservable.toPromise().then((resp: any) => {
            return resp === null
                ? ERR_UNEXPECTED_SERVER
                : this.extractMessageFromResponse(resp);
        });
    }

    /**
     * Returns 'true' if the password is strong enough or a string describing
     * the error otherwise.
     * Note: it takes into account the environment variables for password strength!
     */
    public checkPswdStrength(pswd: string): boolean | string {
        const hasLength =
            pswd.length >= environment.passwordMinimumLength &&
            pswd.length <= environment.passwordMaximumLength;
        if (!hasLength) {
            return (
                'password must be of length between: ' +
                environment.passwordMinimumLength +
                ' and ' +
                environment.passwordMaximumLength
            );
        }
        const hasNumber = environment.passwordMustHaveNumber ? /\d/.test(pswd) : true;
        if (!hasNumber) {
            return 'password must contain numbers';
        }
        const hasUpper = environment.passwordMustHaveUpper ? /[A-Z]/.test(pswd) : true;
        if (!hasUpper) {
            return 'password must contain upper letters';
        }
        const hasLower = environment.passwordMustHaveLower ? /[a-z]/.test(pswd) : true;
        if (!hasLower) {
            return 'password must contain lower letters';
        }
        const hasSymbol = environment.passwordMustHaveSymbol
            ? /[$@$!%*?^&]/.test(pswd)
            : true;
        if (!hasSymbol) {
            return 'password must contain symbols, ex: $@$!&?*&';
        }
        return true;
    }

    private async loginWithJwt(jwtToken: string): Promise<string> {
        const loginObservable: Observable<any> = this.http
            .post(LOGIN_URL, { jwtKey: jwtToken }, { headers: this.headers })
            .pipe(
                catchError((err) => {
                    this.logger.logError(err);
                    return of(this.extractMessageFromResponse(err));
                })
            );
        return await loginObservable.toPromise().then((loginResponse: any) => {
            if (loginResponse === null) {
                return ERR_UNEXPECTED_SERVER;
            }
            /**
             * Check if the user has made a normal login with username & password
             * and if that's so extract the JWT token from backend and save it.
             */
            if (typeof loginResponse === 'string' || loginResponse['success'] === 0) {
                /**
                 * On any kind of error delete the JWT, we don't take any changes
                 */
                localStorage.removeItem(JWT_LOCALSTORAGE_KEY);
                return 'error';
            }
            return '';
        });
    }
}
