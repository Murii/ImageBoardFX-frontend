import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { TopBarView } from '../top-bar/top-bar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ENTER } from '@angular/cdk/keycodes';

export enum ResetPasswordState {
    CODE_STATE = 0,
    NEW_PASSWORD_STATE = 1
}

@Component({
    selector: 'app-reset-credentials',
    templateUrl: './reset-credentials.component.html',
    styleUrls: ['./reset-credentials.component.css'],
    providers: [AuthenticationService]
})
export class ResetCredentialsComponent implements OnInit {
    topBarView: any = TopBarView;

    form: FormGroup = new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email]),
        codeValidator: new FormControl('', [Validators.required]),
        password0: new FormControl('', [
            Validators.required,
            Validators.minLength(environment.passwordMinimumLength),
            Validators.maxLength(environment.passwordMaximumLength)
        ]),
        password1: new FormControl('', [
            Validators.required,
            Validators.minLength(environment.passwordMinimumLength),
            Validators.maxLength(environment.passwordMaximumLength)
        ])
    });

    resetPasswordStateEnum: any = ResetPasswordState;

    /**
     * When in "CODE_STATE" the user must introduce his email
     * after which a request will be made to the back-end for the
     * given email.
     * After that this variable will be set to "NEW_PASSWORD_STATE" forcing the user
     * to introduce the code given in the email by the back-end and the new password he/she wants.
     */
    currentState: ResetPasswordState = ResetPasswordState.CODE_STATE;

    hidePswd1 = true;
    hidePswd2 = true;

    errorMessage: string | undefined = undefined;

    /**
     * The email of the user who's trying to reset the password
     */
    email: string = undefined;

    constructor(
        private snackBar: MatSnackBar,
        private authenticationService: AuthenticationService,
        private router: Router
    ) {}

    ngOnInit() {}

    /**
     * When the user already has an email code let him procede without needing to resend an email code.
     * Note: if the code has expired it will be error as such from back-end
     */
    alreadyHaveEmailCode() {
        this.currentState = ResetPasswordState.NEW_PASSWORD_STATE;
    }

    clickEnter($event: any) {
        if ($event.keyCode === ENTER) {
            if (this.currentState === ResetPasswordState.CODE_STATE) {
                this.preChangePassword();
            } else if (this.currentState === ResetPasswordState.NEW_PASSWORD_STATE) {
                this.clickNewPasswordBtn();
            }
        }
    }

    /**
     * Called when in "CODE_STATE" on click event
     */
    clickCodeStateBtn(): void {
        this.preChangePassword();
    }

    /**
     * Called when in "NEW_PASSWORD_STATE"
     */
    public clickNewPasswordBtn(): void {
        this.errorMessage = undefined;
        if (!this.f.codeValidator.valid || !this.f.password0 || !this.f.password1) {
            return;
        }

        const codeFromEmail: string = this.form.get('codeValidator').value;
        const password0: string = this.form.get('password0').value;
        const password1: string = this.form.get('password1').value;

        if (password0 !== password1) {
            this.errorMessage = 'passwords do not match';
            return;
        }

        const checkPswdStrength = this.authenticationService.checkPswdStrength(password0);
        if (typeof checkPswdStrength === 'string') {
            this.errorMessage = checkPswdStrength;
            return;
        }

        this.authenticationService
            .changePswd(this.email, password0, codeFromEmail)
            .then((res: string) => {
                if (res === '') {
                    this.snackBar.open(
                        'You have successfully reseted your password',
                        'OK',
                        {
                            direction: "ltr",
                            duration: 2200
                        }
                    );
                    setTimeout(() => {
                        this.router.navigate(['']); // after successfully resetting the password redirect the user to home
                    }, 2200);
                } else {
                    this.errorMessage = res;
                }
            });
    }

    get f() {
        return this.form.controls;
    }

    public clickLogin(): void {
        this.router.navigate(['/login']);
    }

    public clickRegister(): void {
        this.router.navigate(['/register']);
    }

    private preChangePassword(): void {
        if (!this.f.email.valid) {
            return;
        }

        const email: string = this.form.get('email').value;
        this.authenticationService.preChangePswd(email).then((res: string) => {
            if (res === '') {
                // this means we're OK to continue
                this.currentState = ResetPasswordState.NEW_PASSWORD_STATE; // proccede to the next step, validating the code from email.
                this.email = email;
            } else {
                this.errorMessage = res;
            }
        });
    }
}
