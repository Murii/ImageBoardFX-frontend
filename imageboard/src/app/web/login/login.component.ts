import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { TopBarView } from '../top-bar/top-bar.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [AuthenticationService]
})
export class LoginComponent implements OnInit {
    topBarView: any = TopBarView;
    /**
     * Set to 'true' or 'false'
     * when the user clicks on the eye
     * icon on the password field.
     * Makes the password visible or invisible.
     */
    hidePswd = true;

    email: string;
    password: string;

    errorMessage: string = undefined;

    constructor(
        private router: Router,
        public authenticationService: AuthenticationService
    ) {}

    ngOnInit() {
        this.authenticationService.trySilentLogin().then((res: string) => {
            if (res !== undefined && res.length === 0)
                // successfully logged in
                this.router.navigate(['/home'], { state: { isLoggedIn: true } });
        });
    }

    public clickResetPassword(): void {
        this.router.navigate(['/reset-password']);
    }

    public clickRegister(): void {
        this.router.navigate(['/register']);
    }

    public login(): boolean {
        if (!this.validateCredentials()) {
            this.errorMessage = 'Invalid credentials format';
            return false;
        }
        this.authenticationService
            .login(this.email, this.password)
            .then((res: string) => {
                if (res !== '') {
                    this.errorMessage = res;
                } else {
                    this.errorMessage = undefined;
                    this.router.navigate(['/home'], { state: { isLoggedIn: true } });
                }
            });
    }

    public validateCredentials(): boolean {
        return (
            this.email !== undefined &&
            this.password !== undefined &&
            this.email.length > 0 &&
            this.password.length > 0
        );
    }
}
