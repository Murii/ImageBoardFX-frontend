import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { TopBarView } from '../top-bar/top-bar.component';

export const ENTER_KEYCODE = 13;

export enum RegisterAccountState {
    ENTER_CREDENTIALS = 0,
    CONFIRM_EMAIL_CODE = 1
}

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css'],
    providers: [AuthenticationService]
})
export class RegisterComponent implements OnInit {
    topBarView: any = TopBarView;

    registerAccountState: any = RegisterAccountState;

    currentRegisterState: RegisterAccountState;

    /** Hides or shows the first password when clicked on the eye icon  */
    hidePswd0 = true;
    /** Hides or shows the second password when clicked on the eye icon  */
    hidePswd1 = true;

    enterCredentialsForm: FormGroup = new FormGroup({
        email: new FormControl('', [Validators.required, Validators.email]),
        username: new FormControl('', [Validators.required, Validators.minLength(3)]),
        password0: new FormControl('', [
            Validators.required,
            Validators.minLength(environment.passwordMinimumLength),
            Validators.maxLength(environment.passwordMaximumLength)
        ]),
        password1: new FormControl('', [
            Validators.required,
            Validators.minLength(environment.passwordMinimumLength),
            Validators.maxLength(environment.passwordMaximumLength)
        ])
    });

    confirmEmailCodeForm: FormGroup = new FormGroup({
        code: new FormControl('', [Validators.required])
    });

    /**
     * Any errors will be show to the user using this
     */
    errorMessage: string | undefined = undefined;

    email: string;

    constructor(
        private authenticationService: AuthenticationService,
        private router: Router
    ) {
        this.currentRegisterState = RegisterAccountState.ENTER_CREDENTIALS;
    }

    ngOnInit() {}

    register(): void {
        if (this.enterCredentialsForm.invalid) {
            return;
        }
        const email: string = this.enterCredentialsForm.get('email').value;
        this.email = email;
        const username: string = this.enterCredentialsForm.get('username').value;
        const password0: string = this.enterCredentialsForm.get('password0').value;
        const password1: string = this.enterCredentialsForm.get('password1').value;

        if (password0 !== password1) {
            this.errorMessage = 'passwords do not match';
            return;
        }

        const checkPswdStrength = this.authenticationService.checkPswdStrength(password0);
        if (typeof checkPswdStrength === 'string') {
            this.errorMessage = checkPswdStrength;
            this.enterCredentialsForm.get('password0').setErrors({ incorrect: true });
            this.enterCredentialsForm.get('password1').setErrors({ incorrect: true });
            return;
        } else {
            this.errorMessage = undefined;
            this.enterCredentialsForm.get('password0').setErrors(null);
            this.enterCredentialsForm.get('password1').setErrors(null);
        }

        this.authenticationService
            .register(email, username, password0)
            .then((res: string) => {
                this.errorMessage = undefined;
                if (res === '') {
                    this.currentRegisterState = RegisterAccountState.CONFIRM_EMAIL_CODE;
                } else {
                    this.errorMessage = res;
                    /*
                     When the 'if' from below executes then it means the user exists  and he just has to confirm his email code. 
                     This could happen if the user has entered wrongly his code for the first time then he wants to retry the process
                    **/
                    if (res === 'Email code not validated') {
                        this.errorMessage = undefined;
                        this.currentRegisterState =
                            RegisterAccountState.CONFIRM_EMAIL_CODE;
                    }
                }
            });
    }

    confirmCode(): void {
        if (this.confirmEmailCodeForm.invalid) {
            return;
        }
        this.authenticationService
            .confirmRegistration(this.email, this.confirmEmailCodeForm.get('code').value)
            .then((res: string) => {
                if (res === '') {
                    this.navigateHome();
                } else {
                    this.errorMessage = res;
                }
            });
    }

    onKeyUp($event: any): void {
        if ($event.keyCode === ENTER_KEYCODE) {
            if (this.currentRegisterState === RegisterAccountState.ENTER_CREDENTIALS) {
                this.register();
            } else if (
                this.currentRegisterState === RegisterAccountState.CONFIRM_EMAIL_CODE
            ) {
                this.confirmCode();
            }
        }
    }

    public clickResetPassword(): void {
        this.router.navigate(['/reset-password']);
    }

    public clickLogin(): void {
        this.router.navigate(['/login']);
    }

    get f() {
        if (this.currentRegisterState === RegisterAccountState.ENTER_CREDENTIALS) {
            return this.enterCredentialsForm.controls;
        } else if (
            this.currentRegisterState === RegisterAccountState.CONFIRM_EMAIL_CODE
        ) {
            return this.confirmEmailCodeForm.controls;
        }
    }

    navigateHome(): void {
        this.router.navigate(['/home']);
    }
}
