import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ImageService } from 'src/app/services/image.service';
import { NewPostComponent } from '../new-post/new-post.component';
import { TopBarComponent, TopBarView } from '../top-bar/top-bar.component';
import { IPost } from './post/post.interface';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    public topBarView: any = TopBarView;

    public posts: Array<IPost> = new Array<IPost>();

    public showLoadingSpinner: boolean = false;

    /**
     * Holds the total number of posts
     * which we have stored on the back-end.
     * Used to be displayed to the user
     */
    public totalNumberOfPosts: number = 0;

    public currentPage: number = 1;

    /**
     * How many posts per batch do we need to fetch from the backend
     * and they show to the user
     */
    public readonly NUMBER_OF_POSTS_TO_DISPLAY: number = 3;

    @ViewChild('topbar')
    private topBarComponent: TopBarComponent;

    constructor(
        private authenticationService: AuthenticationService,
        private imageService: ImageService
    ) {}

    ngOnInit() {
        this.authenticationService.trySilentLogin().then((res: string) => {
            if (res !== undefined && res.length === 0)
                // successfully logged in
                this.topBarComponent.isLoggedIn = true;
        });

        this.imageService.showLoadingSpinner.subscribe((show: boolean) => {
            this.showLoadingSpinner = show;
        });

        this.requestAllPosts();

        NewPostComponent.onNewPost$.subscribe(() => {
            this.requestAllPosts();
        });
    }

    public pageBoundsCorrection(event$: any): void {
        console.error('page bounds: ', event$);
    }

    public pageChanged(page: any): void {
        const postsIndex: number = (page - 1) * this.NUMBER_OF_POSTS_TO_DISPLAY;

        this.currentPage = page;
        this.fetchNewPosts(postsIndex, this.NUMBER_OF_POSTS_TO_DISPLAY);
        window.scrollTo(0, 0); // reset scroll position to top after changing page
    }

    /**
     * This method will try to fetch new posts and at the same time show a loading
     * spinner
     */
    private fetchNewPosts(startIndex: number, numberOfPostsToShow: number): void {
        this.imageService
            .getPosts(startIndex, numberOfPostsToShow)
            .then((posts: Array<any>) => {
                this.posts = this.convertUTCTimestampToLocalTime(posts);
            });
    }

    /**
     * This function will request all the posts from back-end and set
     * the current page to the latest.
     */
    private requestAllPosts(): void {
        // Get the number of posts then the post themselfs
        this.imageService.getNumberOfPost().then((total: number) => {
            total += 1;
            while (total % this.NUMBER_OF_POSTS_TO_DISPLAY != 0) {
                total++;
            }
            this.totalNumberOfPosts = total;
            this.currentPage = Math.floor(total / this.NUMBER_OF_POSTS_TO_DISPLAY);

            this.imageService
                .getPosts(total - 3, this.NUMBER_OF_POSTS_TO_DISPLAY)
                .then((posts: Array<any>) => {
                    this.posts = this.convertUTCTimestampToLocalTime(posts);
                });
        });
    }

    private convertUTCTimestampToLocalTime(posts: Array<any>): Array<IPost> {
        posts.forEach((p) => {
            // here we convert from UTC timestamp to local time
            const date: Date = new Date(p['creationDate']);
            p['creationDate'] =
                date.getHours() +
                ':' +
                date.getMinutes() +
                ', ' +
                date.getFullYear() +
                '-' +
                date.getMonth() +
                '-' +
                date.getDay();
        });
        return posts;
    }
}
