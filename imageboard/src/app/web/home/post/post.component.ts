import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input("title")
  public title: string;

  @Input("author")
  public author: string;

  @Input("description")
  public description: string;

  @Input("imageData")
  public imageData: string;

  @Input("dataType")
  public dataType: string;

  @Input("tags")
  public tags: string[];

  @Input("creationDate")
  public creationDate: string;

  public imageSource: any = undefined;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.dataType = this.dataType || "image/png";
    this.imageSource = this.sanitizer.bypassSecurityTrustResourceUrl(`data:${this.dataType};base64,${this.imageData}`);
  }


}
