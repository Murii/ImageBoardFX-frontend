export interface IPost {
    id: number;
    title: string;
    author: string;
    dataType: string;
    imageData: string;
    description: string;
    tags: string[];
    creationDate: string;
}