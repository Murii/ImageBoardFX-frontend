import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { USERNAME_LOCALSTORAGE_KEY } from 'src/app/services/authentication.service';
import { ImageService } from 'src/app/services/image.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-new-post',
    templateUrl: './new-post.component.html',
    styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
    /**
     * Used to tell us when a new post has been sent to the back-end.
     * We listen to this in home component so we can refresh with the latest post
     */
    public static onNewPost$: EventEmitter<boolean> = new EventEmitter<boolean>();

    public readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    public title: FormControl = new FormControl('', [
        Validators.required,
        Validators.minLength(environment.minimumNewPostTitleLength),
        Validators.maxLength(environment.maximumNewPostTitleLength)
    ]);
    public description: FormControl = new FormControl('', []);
    public chips: FormControl = new FormControl('', [Validators.required]);

    public selectedFileFormControl: FormControl = new FormControl('', [
        Validators.required
    ]);

    /**
     * Gets set to true when the loaded image size is grater
     * than the maximum allowed image size to send to back-end
     */
    public imageExecedesMaximumSize: boolean = false;

    /**
     * Holds the list of the tags associated with this new post
     */
    public tagList: Array<string> = new Array<string>();

    public imageURL: any = undefined;

    private selectedFile: File = undefined;

    constructor(
        private snackBar: MatSnackBar,
        private dialogRef: MatDialogRef<NewPostComponent>,
        private imageService: ImageService
    ) {}

    ngOnInit(): void {}

    /**
     * Used to load an image from local disk
     * @param imageInput the selected image from local disk
     */
    selectImage(imageInput: any): void {
        const reader: FileReader = new FileReader();
        this.selectedFile = imageInput.files[0];
        reader.readAsDataURL(this.selectedFile);
        reader.onload = () => {
            this.imageURL = reader.result;
            if (this.selectedFile.size > environment.maximumNewPostImageSize) {
                this.selectedFileFormControl.setErrors({ incorrect: true });
                this.imageExecedesMaximumSize = true;
            } else {
                this.selectedFileFormControl.setErrors(null);
                this.imageExecedesMaximumSize = false;
            }
        };
        this.selectedFileFormControl.setErrors(null);
    }

    /**
     * Called when the users wants to add a new tag
     */
    addTag($event: MatChipInputEvent): void {
        if (!$event.value || this.tagList.length >= environment.maximumTagsPerPost) {
            return;
        }
        const value: string = $event.value;
        const trimmedValue: string = value.trim();
        if (trimmedValue === '' || this.tagList.indexOf(trimmedValue) !== -1) {
            return;
        }
        this.tagList.push(trimmedValue);

        $event.input.value = '';
    }

    /**
     * Called when the users wants to remove a tag
     */
    removeTag(tag: string): void {
        const index: number = this.tagList.indexOf(tag);
        if (index >= 0) {
            this.tagList.splice(index, 1);
        }
    }

    onFinishBtnClick(): void {
        this.description.markAsTouched();
        this.title.markAsTouched();
        this.chips.markAsTouched();
        this.selectedFileFormControl.markAsTouched();
        if (
            this.title.invalid ||
            this.description.invalid ||
            this.chips.invalid ||
            this.selectedFileFormControl.invalid
        ) {
            return;
        }
        this.imageService
            .uploadPost(
                this.title.value,
                localStorage.getItem(USERNAME_LOCALSTORAGE_KEY),
                this.selectedFile,
                this.description.value,
                this.tagList
            )
            .then((resp: string) => {
                if (resp.length === 0) {
                    this.snackBar.open('Your post is live', '', {
                        duration: 3000,
                        direction: "ltr"
                    });
                    NewPostComponent.onNewPost$.emit(true);
                }
            });

        this.dialogRef.close();
    }

    getTitleErrorMessage(): string {
        if (this.title.hasError('required')) {
            return 'Expected title';
        } else if (this.title.hasError('minlength')) {
            return (
                "Title's length must be bigger than " +
                environment.minimumNewPostTitleLength +
                ' characters '
            );
        } else if (this.title.hasError('maxlength')) {
            return (
                "Title's length must be shorter than " +
                environment.maximumNewPostTitleLength +
                ' characters '
            );
        }
    }

    checkSelectedImageSizeErrorMessage(): string {
        if (this.imageExecedesMaximumSize) {
            return (
                'Image size of ' +
                this.selectedFile.size / 1000000 +
                'mb is bigger than maximum allowed of' +
                environment.maximumNewPostImageSize / 1000000 +
                'mb'
            );
        }
        return '';
    }

    getSelectedImageErrorMessage(): string {
        if (
            this.selectedFile === undefined ||
            this.selectedFileFormControl.hasError('required')
        ) {
            return 'Expected image input';
        }
        return '';
    }

    getChipsErrorMessage(): string {
        if (this.tagList.length === 0) {
            return 'Expected tags';
        } else if (this.tagList.length >= environment.maximumTagsPerPost) {
            return 'Cannot have more than ' + environment.maximumTagsPerPost + ' tags';
        }
    }
}
