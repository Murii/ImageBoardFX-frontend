import { AfterContentInit, Component, Input, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService, JWT_LOCALSTORAGE_KEY, USERNAME_LOCALSTORAGE_KEY } from '../../services/authentication.service';
import { NewPostComponent } from '../new-post/new-post.component';

/**
 * Change the top bar based on the current view
 */
export enum TopBarView {
    GENERIC = 1,
    RESET_PASSWORD = 2,
    HOME
}

@Component({
    selector: 'app-top-bar',
    templateUrl: './top-bar.component.html',
    styleUrls: ['./top-bar.component.css'],
    providers: [AuthenticationService]
})
export class TopBarComponent implements AfterContentInit {
    /**
     * When this is set to true then we change the view
     * designed specially for logged users
     */
    public isLoggedIn: boolean = false;

    public topBarView: any = TopBarView;

    @Input()
    currentView: TopBarView = TopBarView.HOME;

    /* 
       Optional right now used only in RESET_PASSWORD. 
       This text shows in the middle of the bar
    */
    @Input()
    text: string;

    @ViewChild(MatInput)
    searchInput: MatInput;

    constructor(
        private router: Router,
        private dialog: MatDialog,
        private snackBar: MatSnackBar,
        public authenticationService: AuthenticationService
    ) {}

    ngAfterContentInit() {
        // Check if we're in HOME view and if the user has logged in
        if (this.currentView === TopBarView.HOME) {
            this.isLoggedIn = window.history.state.isLoggedIn || false;
        }
    }

    clickedSearchIcon() {
        this.searchForPost();
    }

    onInputSearchType() {
        this.searchForPost();
    }

    onNewPostClick($event: any): void {
        this.dialog.open(NewPostComponent, {
            width: window.outerWidth / 2.5 + 'px',
            height: window.outerHeight / 2 + 'px',
            maxHeight: "800px",
            disableClose: true
        });
    }

    clickLogin() {
        this.router.navigate(['/login']);
    }

    clickedLogout() {
        localStorage.removeItem(JWT_LOCALSTORAGE_KEY);
        localStorage.removeItem(USERNAME_LOCALSTORAGE_KEY);
        this.isLoggedIn = false;
        this.snackBar.open("Successfully logged out", '', {direction: "ltr", duration: 600});
    }

    clickRegister() {
        this.router.navigate(['/register']);
    }

    private searchForPost(): void {

    }
}
