// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,

    backendIpAddress: 'http://127.0.0.1:12338',

    usernameMinLength: 2,
    usernameMaxLength: 8,

    passwordMinimumLength: 8,
    passwordMaximumLength: 16,
    passwordMustHaveNumber: true,
    passwordMustHaveUpper: true,
    passwordMustHaveLower: true,
    passwordMustHaveSymbol: true,

    appVersion: 0.1,

    maximumTagsPerPost: 6,
    maximumNewPostTitleLength: 32,
    minimumNewPostTitleLength: 2,
    maximumNewPostImageSize: 10000000, // 10mb, this limit has been set on back-end
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
